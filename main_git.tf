# specifing the provider for terraform to connect to. This requires the region and senstive keys generated in AWS account
provider "aws" {
    region = "us-east-1"
    access_key = "access_key" # sensitive
    secret_key = "secret_key" # sensitive
}

data "aws_availability_zones" "available" {} # allows terraform to store data from available data sources. In this case, it is storing available cloud availability zones e.g., US West (Northern California)

# here we are creating a virtual network called a vpc (Virtual Private Cloud). This will be used to run resources virtually, in this case, a relational database

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.77.0"

  name                 = "education"
  cidr                 = "10.0.0.0/16" # cidr block is a notion to represent a range of IP addresses
  azs                  = data.aws_availability_zones.available.names # using availability zones returned in data block above
  public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"] # the IP addresses for each subnets are being defined
  enable_dns_hostnames = true
  enable_dns_support   = true
}

# making a subnet group resource with a reference name of "education". the subnet id's are being taken from the vpc module property "public_subnets"
resource "aws_db_subnet_group" "education" {
  name       = "education"
  subnet_ids = module.vpc.public_subnets

  tags = { # giving the resource a tag
    Name = "Education"
  }
}

# it is necessary to set up a security group for our VPC. A security group acts as virtual firewall and controls incoming and outgoing traffic using defined inbound rules and outbound rules.
resource "aws_security_group" "rds" { 
  name   = "education_rds"
  vpc_id = module.vpc.vpc_id

  # defining inbound rules
  ingress {
    from_port   = 5432 # defining range of ports
    to_port     = 5432
    protocol    = "tcp" # protocol set to Transmission Control Protocol
    cidr_blocks = ["0.0.0.0/0"]
  }

  # defining outbound rules
  egress {
    from_port   = 5432 # defining range of ports
    to_port     = 5432
    protocol    = "tcp" # protocol set to Transmission Control Protocol
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "education_rds"
  }
}

# here we are setting up an aws parameter group to specify how the database is configured
resource "aws_db_parameter_group" "education" {
  name   = "education"
  family = "postgres14"

  parameter {
    name  = "log_connections"
    value = "1"
  }
}

# creating the database instance
resource "aws_db_instance" "education" {
  identifier             = "education"
  instance_class         = "db.t3.micro"
  allocated_storage      = 5 # GiB
  engine                 = "postgres"
  engine_version         = "14.1"
  username               = "matias_user"
  password               = var.db_password # specifying password in variable terraform file
  db_subnet_group_name   = aws_db_subnet_group.education.name # referencing terraform subnet resource block
  vpc_security_group_ids = [aws_security_group.rds.id] # specifying security using terraform resource block id
  parameter_group_name   = aws_db_parameter_group.education.name
  publicly_accessible    = true
  skip_final_snapshot    = true
}


