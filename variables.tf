# making a variable file to avoid sensitive information being share while calling variables in the main.tf file

variable "region" {
  default     = "us-east-1"
  description = "AWS region"
}

variable "db_password" {
  description = "Enter Password"
  sensitive   = true
}
