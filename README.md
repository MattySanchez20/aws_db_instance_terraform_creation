# Creating A Cloud Server to Host a Postgres Database

## Goal

The motivation for this project was to have a playground for SQL queries. To do this, a database with the capacity to store a decent amount of data had to be created. A goal was made to create said database on a cloud server using AWS. The best way to ensure the database could be easily managed automatically was to use Terraform.

## Theory

Terraform:

An Infrastructure-As-Code Software that allows for automation of resource management, creating and deletion. Terraform was used to create the database on AWS (details shown in architecture section). The language used in Terraform is called HashiCorp Configuration Language and is a declarative language. This means that the code written in Terraform is a representation of the desired final state of the infrastructure. So if a terraform file is executed, it looks for changes that need to be made in the infrastructure, if there are none, the code will be executed. This differs from other conventional languages because if it were to be coded using, for example, python, running the .py file 10 times would result in 10 infrastructures.

Cloud Architecture:

To create the database on the cloud, it was necessary to create the infrastructure on AWS. The main components of the cloud infrastructure were as follows:

- VPC
- Subnet
- Security Group
- AWS DB Instance

VPC: A Virtual Private Cloud is a virtual network used to launch AWS resources such as an S3 Bucket, or a database.

Subnet: A subnet is a range of IP addresses within the VPC.

Security Group: A security group acts as virtual firewall and controls incoming and outgoing traffic using defined inbound rules and outbound rules.

AWS DB Instance: A resource available in AWS that is used to create a database. The important parameters are the engine (postgres), the engine_version (14.1), and the instance_class (db.t3.micro: a type of database available in AWS free tier).
